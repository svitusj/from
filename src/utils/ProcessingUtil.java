package utils;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import data.ModelElement;
import data.Query;
import opennlp.tools.lemmatizer.SimpleLemmatizer;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.tokenize.WhitespaceTokenizer;

public class ProcessingUtil {

	/* Variables for the linguistic models */
	private final String posModelRoute = "/resources/en-pos-maxent.bin";
	private final String dictRoute = "/resources/en-lemmatizer.dict";
	private POSTaggerME tagger;
	private SimpleLemmatizer lemmatizer;
	
	public ProcessingUtil() {
			tagger = createPosTagger();
			lemmatizer = createLemmatizer();
	}

	public void process(Query query) {
		
		// Process the query
		basicProcessing(query);
		posTag(query);
		lemmatize(query);
		
	}
	
	public void process(ModelElement element) {
		
		// Process the individual
		basicProcessing(element);
		posTag(element);
		lemmatize(element);
		
	}
	
	/* Automatic Linguistic Preprocessing methods */
	
	private void basicProcessing(ModelElement element)
	{
		
		// Tokenize the string
        String[] individualWords = WhitespaceTokenizer.INSTANCE.tokenize(element.getText());
                
        ArrayList<String> processedInd = new ArrayList<String>();

        // Add the words to the individual processed list
        for(String word : individualWords)
        {
        	// Lowercase the word first
        	word = word.toLowerCase();
        	processedInd.add(word);
        }
        
        element.setProcessedText(processedInd);
        
	}
	
	private void basicProcessing(Query query) {
		
		String[] queryWords = WhitespaceTokenizer.INSTANCE.tokenize(query.getText());
        ArrayList<String> processedQuery = new ArrayList<String>();
        
        // Add the words to the individual processed list
        for(String word : queryWords)
        {
        	// Lowercase the word first
        	word = word.toLowerCase();
        	processedQuery.add(word);
        }
        
        query.setProcessedText(processedQuery);
	}
	
	@SuppressWarnings("deprecation")
	private void posTag(ModelElement element)
    {
		/* START POS TAGGING */

		// Get the words as array
		ArrayList<String> individualWords = element.getProcessedText();

		// Get the array of tags
		List<String> indT = tagger.tag(individualWords);

		ArrayList<String> indTags = new ArrayList<String>();
		indTags.addAll(indT);

		// Store the tags in the proper token variable
		element.setPosTags(indTags);

		/* END POS TAGGING */
    }

	@SuppressWarnings("deprecation")
	private void posTag(Query query)
    {
		/* START POS TAGGING */

		// Get the words as array
		ArrayList<String> queryWords = query.getProcessedText();

		// Get the array of tags
		List<String> queryT = tagger.tag(queryWords);
		ArrayList<String> queryTags = new ArrayList<String>();
		queryTags.addAll(queryT);

		// Store the tags in the proper token variable
		query.setPosTags(queryTags);

		/* END POS TAGGING */
     
    }

	private void lemmatize(ModelElement element) 
	{

		// Get the words as array
		ArrayList<String> words = element.getProcessedText();

		// Get the array of tags
		ArrayList<String> tags = element.getPosTags();

		// Create the array of lemmas
		ArrayList<String> lemmas = new ArrayList<String>();
		int index = 0;

		// For all the words of the processed individual
		for(String word : words) {
			// Lemmatize word according to its postag
			String lemma = lemmatizer.lemmatize(word, tags.get(index));
			index++;

			// Save the lemma
			lemmas.add(lemma);
		}

		element.setLemmas(lemmas);
		element.setProcessedText(lemmas);
		
		LinkedHashSet<String> terms = new LinkedHashSet<String>();
		terms.addAll(lemmas);
		element.setTerms(terms);
			
	}

	private void lemmatize(Query query) 
	{
			
		// Get the words as array
		ArrayList<String> words = query.getProcessedText();

		// Get the array of tags
		ArrayList<String> tags = query.getPosTags();

		// Create the array of lemmas
		ArrayList<String> lemmas = new ArrayList<String>();
		int index = 0;

		// For all the words of the processed individual
		for(String word : words)
		{
			// Lemmatize word according to its postag
			String lemma = lemmatizer.lemmatize(word, tags.get(index));
			index++;

			// Save the lemma
			lemmas.add(lemma);
		}

		query.setLemmas(lemmas);
		query.setProcessedText(lemmas);
		
		LinkedHashSet<String> terms = new LinkedHashSet<String>();
		terms.addAll(lemmas);
		query.setTerms(terms);
			
	}
	
	/* POS Tagger and Lemmatizer methods */
	
	private POSTaggerME createPosTagger() {
		
		try {
			
			// Create the POS Model from resource stream
			InputStream is = this.getClass().getResourceAsStream(posModelRoute);
			final POSModel model = new POSModel(is);
	        
			// Create the POS tagger from the model
			POSTaggerME tagger = new POSTaggerME(model);
			
			// Close the resource stream
			is.close();
			return tagger;
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}
	
	private SimpleLemmatizer createLemmatizer() {
		
		try {
			
			// Get the dictionary as stream
			InputStream is = this.getClass().getResourceAsStream(dictRoute);
			
			// Create the lemmatizer
			SimpleLemmatizer lemmatizer = new SimpleLemmatizer(is);
			
			// Close the stream
			is.close();
			
			return lemmatizer;
		
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}
}
