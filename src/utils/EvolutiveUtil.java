package utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import data.Model;
import data.ModelElement;
import data.ModelFragment;

public class EvolutiveUtil {
	
	private int populationSize;
	private ArrayList<ModelFragment> population;
	private int iterations; 
	
	public EvolutiveUtil(int populationSize, ArrayList<Model> models, int iterations) {
		
		this.populationSize = populationSize;
		this.population = initialPopulationGeneration(populationSize, models);
		this.iterations = iterations;
	}
	
	/* Evolutionary Algorithm */
	
	public ArrayList<ModelFragment> evolve() {
		for(int i = 0; i < iterations; i++)	population = breed(population);
		Collections.sort(population);
		return population;
	}

	private ArrayList<ModelFragment> breed(ArrayList<ModelFragment> population) {
		
		ArrayList<ModelFragment> newPopulation = new ArrayList<ModelFragment>();
		
		for(int i = 0; i < populationSize; i++) {
			
			ModelFragment firstParent = selection(population);
			Model secondParent = selection(population, firstParent).getParent();
			
			ModelFragment individual = crossover(firstParent, secondParent);
			individual = mutation(individual);
			
			newPopulation.add(individual);
			
		}
		
		return newPopulation;
	}
	
	
	/* Population methods */
	
	private ModelFragment fragmentGeneration(int id, Model model) {
		
		Random randomizer = new Random();
		
		// Generate seed element
		int seed = randomizer.nextInt(model.size());
		ModelElement seedElement = model.getElements().get(seed);		
		ModelElement neighbor = seedElement.getNeighbor();
		
		// Generate the list of elements
		ArrayList<ModelElement> elements = new ArrayList<ModelElement>();
		elements.add(seedElement);
		
		// Populate the list of elements
		int iterations = randomizer.nextInt(model.size());
		for (int i = 0; i < iterations; i++) {
			
			ModelElement previous = seedElement;
			seedElement = neighbor;
			elements.add(seedElement);
			neighbor = seedElement.getNeighbor(previous);
			
			if(neighbor.equals(null)) {
				break;				
			}
			
		}
		
		// Generate the fragment with the list of elements
		ModelFragment fragment = new ModelFragment(id, elements, model);
		return fragment;
		
	}
	
	private ArrayList<ModelFragment> initialPopulationGeneration(int populationSize, ArrayList<Model> models) {
		
		ArrayList<ModelFragment> initialPopulation = new ArrayList<ModelFragment>();
		Random randomizer = new Random();
		
		for(int i = 0; i < populationSize; i++) {
			
			int parent = randomizer.nextInt(models.size());
			Model parentModel = models.get(parent);
			ModelFragment individual = fragmentGeneration(i, parentModel);
			initialPopulation.add(individual);
			
		}
		
		return initialPopulation;
		
	}
	
	/* Genetic Algorithm Operations */
	
	private ModelFragment selection(ArrayList<ModelFragment> population) {
		
		Random randomizer = new Random();
		int seed = randomizer.nextInt(population.size());
		return population.get(seed);
		
	}
	
	private ModelFragment selection(ArrayList<ModelFragment> population, ModelFragment distinct) {
		
		Random randomizer = new Random();
		int seed = randomizer.nextInt(population.size());
		ModelFragment chosen = population.get(seed);
		
		while(chosen.equals(distinct)) {
			
			seed = randomizer.nextInt(population.size());
			chosen = population.get(seed);
		
		}
		
		return chosen;
		
	}
	
	public ModelFragment crossover(ModelFragment firstParent, Model secondParent) {
		
		ModelFragment newIndividual;
		
		if(firstParent.isInsideModel(secondParent)) {
			newIndividual = firstParent;
			newIndividual.setParent(secondParent);
		} else {
			newIndividual = firstParent;
		}
		
		return newIndividual;
		
	}
	
	public ModelFragment mutation(ModelFragment individual) {
		
		Random randomizer = new Random();
		boolean operation = randomizer.nextBoolean();
		
		if(operation) {
			
			ModelElement addition = individual.findAdditionCandidate();
			individual.getElements().add(addition);
			
		} else {
			
			ModelElement removal = individual.findRemovalCandidate();
			individual.getElements().remove(removal);
		}
		
		return individual;
		
	}
	
	/* Getters */

	public ArrayList<ModelFragment> getPopulation() {
		return population;
	}

	public int getPopulationSize() {
		return populationSize;
	}

	public int getIterations() {
		return iterations;
	}
	
	
}
