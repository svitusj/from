package utils;

import data.Model;
import data.ModelElement;
import data.ModelFragment;

public class EncodingUtil {
	
	public static boolean[] encodeModelFragment(ModelFragment fragment){
		
		// Get the parent
		Model parent = fragment.getParent();		
		
		// Create encoding array with parent size, filled with false values 
		boolean[] encoding = new boolean[parent.size()];
		for(int i = 0; i < encoding.length; i++) 
			encoding[i] = false;
		
		// For each element in the parent, check whether the element is in the fragment
		// If the element is in the fragment, then set the encoding value to true
		for(int elementPos = 0; elementPos < parent.size(); elementPos++){
			
			// Get the element in the parent
			ModelElement element = parent.getElements().get(elementPos);
			
			// Check and modify encoding if necessary
			if(fragment.getElements().contains(element))
				encoding[elementPos] = true;
			
		}
		
		// Return the encoding
		return encoding;
		
	}

}
