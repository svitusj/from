package utils;

import data.ModelFragment;

public class EvaluationUtil {
	
	public static void evaluateFragment(ModelFragment fragment, ModelFragment oracle){
		
		// Get the encoding of both fragment and oracle
		boolean[] fragmentEncoding = EncodingUtil.encodeModelFragment(fragment);
		boolean[] oracleEncoding = EncodingUtil.encodeModelFragment(oracle);
		
		// Compare fragment with oracle
		// Obtain TP, TF, FP, FN
		// For each position in the obtained encodings, compare the values
		
		int TP = 0; // true in both the fragment and the oracle
		int TN = 0; // false in both the fragment and the oracle
		int FP = 0; // true in the fragment, false in the oracle
		int FN = 0; // false in the fragment, true in the oracle
		
		for(int pos = 0; pos < fragmentEncoding.length; pos++){
			
			if(fragmentEncoding[pos] && oracleEncoding[pos]) TP++;
			else if(!fragmentEncoding[pos] && !oracleEncoding[pos]) TN++;
			else if(fragmentEncoding[pos] && !oracleEncoding[pos]) FP++;
			else if(!fragmentEncoding[pos] && oracleEncoding[pos]) FN++;
			
		}
		
		
		// Calculate the derived measurements
		double precision = TP/(TP+FP);
		double recall = TP/(TP+FN);
		double fmeasure = 2*((precision*recall)/(precision+recall));
		
		
		// Print the measurement values for the fragment
		System.out.println("Fragment " + fragment.getId() + "\n" +
						   "TP = " + TP + "\n" +
						   "TN = " + TN + "\n" +
						   "FP = " + FP + "\n" +
						   "FN = " + FN + "\n" +
						   "Precision = " + precision + "\n" +
						   "Recall = " + recall + "\n" +
						   "F-Measure = " + fmeasure + "\n");
		
	}

}
