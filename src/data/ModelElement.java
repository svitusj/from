package data;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Random;

public class ModelElement {
	
	private int id;
	private String text;
	private ArrayList<String> processedText;
	private ArrayList<String> posTags;
	private ArrayList<String> lemmas;
	private LinkedHashSet<String> terms;
	private ArrayList<ModelElement> connections;
	
	public ModelElement(int id, String text) {
		
		this.id = id;
		this.text = text;
		this.connections = new ArrayList<ModelElement>();
		
	}
	
	public void addConnection(ModelElement connection) {
		this.connections.add(connection);
	}
	
	public ModelElement getNeighbor() {
		
		if (connections.size() == 1) return connections.get(0); // Only one valid connection
		else {
			Random randomizer = new Random();
			int seed = randomizer.nextInt(connections.size());
			return connections.get(seed);
		}
		
	}
	
	public ModelElement getNeighbor(ModelElement distinct) {
		
		if (connections.size() == 1) return null; // No more valid connections
		else {
			
			Random randomizer = new Random();
			int seed = randomizer.nextInt(connections.size());
			ModelElement seedElement = connections.get(seed);
			
			while(seedElement.equals(distinct)) {
				
				seed = randomizer.nextInt(connections.size());
				seedElement = connections.get(seed);
			
			}
			
			return seedElement;
		}
	}
	
	/* Other methods */
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ModelElement other = (ModelElement) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	

	/* Getters and setters */
	
	public int getId() {
		return id;
	}

	public String getText() {
		return text;
	}

	public ArrayList<String> getProcessedText() {
		return processedText;
	}
	
	public void setProcessedText(ArrayList<String> processedText) {
		this.processedText = processedText;
	}
	
	public ArrayList<String> getPosTags() {
		return posTags;
	}

	public void setPosTags(ArrayList<String> posTags) {
		this.posTags = posTags;
	}

	public ArrayList<String> getLemmas() {
		return lemmas;
	}

	public void setLemmas(ArrayList<String> lemmas) {
		this.lemmas = lemmas;
	}
	
	public ArrayList<ModelElement> getConnections() {
		return connections;
	}
	
	public LinkedHashSet<String> getTerms() {
		return terms;
	}

	public void setTerms(LinkedHashSet<String> terms) {
		this.terms = terms;
	}
	
}
